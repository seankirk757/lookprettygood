Project Brief

Rebuild the checkout payment info form on http://www.lookfantastic.com to make it easier for customers to enter the relevant information across different devices. I’m looking for you to think about the type of data that’s being entered, and to bear in mind that this for is used by customers all over the world. Not just from the UK. 

Remember that if people can’t enter their payment details successfully, they can’t buy anything and we don’t have a business. 

This feature must work across all supported devices and browsers. Graceful degradation is allowed but you decisions must be backed up with good reasons. You must be able to demonstrate the prototype on a several devices (emulators or physical devices). You must show you have used version control where appropriate.

