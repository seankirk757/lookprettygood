<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" style="text/css" href="styles/css/main.css">
<body>
	<h1>Billing Information</h1>
	<article class="row">
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Card Number</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo (int)($_POST['cardNo']); ?>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Name on Card</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['name']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Expiry Month</p>
			</div>
			<div class="column large-6 med-6 small-"> 
				<div class="formpagebox2">
					&nbsp;<?php echo (int)($_POST['month']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Expiry Year</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo (int)($_POST['year']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">CSC Code</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo (int)($_POST['code']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Company Name</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['company']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">House Name/Number</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['houseNo']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Address 1</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['address1']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Address 2</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['address2']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Town/City</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['city']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">County</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['county']); ?>
				</div>
			</div>
		</section>
		<section class="row">
			<div class="column large-6 med-6 small-">
				<p class="box formpagebox">Country</p>
			</div>
			<div class="column large-6 med-6 small-">
				<div class="formpagebox2">
					&nbsp;<?php echo htmlspecialchars($_POST['country']); ?>
				</div>
			</div>
		</section>
	</article>
</body>
</html>





