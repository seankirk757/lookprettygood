var a1 = document.getElementById('cardForm1'); //gets id for card number
var a2 = document.getElementById('cardForm2'); //gets id for card name
var a3 = document.getElementById('cardForm3'); //gets id for card expiry month
var a4 = document.getElementById('cardFormYear'); //gets id for card expiry year
var a5 = document.getElementById('cardForm4'); //gets id for card security code
var b = document.getElementById('paypalForm'); //gets id for paypal info box
var c = document.getElementById('alipayForm'); //gets id for alipay info box
var d = document.getElementById('addForm');  //gets id for delivery information
var e = document.getElementById('sameAdd');  //gets id for same address button
var f = document.getElementById('diff');  //gets id for different address button

var x1 = document.getElementById('bill1');  //gets id for billing address heading
var x2 = document.getElementById('bill2');  //gets id for address button div
var x3 = document.getElementById('bill3');  //gets id for differnt address article

var form11 = document.getElementById('cardNo');  //gets id for card number input 
var form12 = document.getElementById('name');  //gets id for card name input
var form13 = document.getElementById('month');  //gets id for expiry month input
var form14 = document.getElementById('year');  //gets id for expiry year input
var form15 = document.getElementById('code');  //gets id for code input

var form21 = document.getElementById('houseNo');  //gets id for house number input
var form22 = document.getElementById('address1');  //gets id for address line 1 input
var form23 = document.getElementById('city');  //gets id for city input
var form24 = document.getElementById('country');  //gets id for country input
var form25 = document.getElementById('company');  //gets id for company input
var form26 = document.getElementById('address2');  //gets id for address line 2 input
var form27 = document.getElementById('county');  //gets id for county input

/*
 * When user clicks credit card payment
 * method, the form is displayed and the
 * paypal and alipay info boxes are hidden 
 */
function card() {
	a1.style.display = 'block';
	a2.style.display = 'block';
	a3.style.display = 'block';
	a4.style.display = 'block';
	a5.style.display = 'block';
	b.style.display = 'none';
	c.style.display = 'none';
	d.style.display = 'block';
	x1.style.display = 'block';
	x2.style.display = 'block';
	x3.style.display = 'none';
	e.style.display = 'none';
	f.style.display = 'block';
	form11.required = true;
	form12.required = true;
	form13.required = true;
	form14.required = true;
	form15.required = true;
	form21.required = false;
	form22.required = false;
	form23.required = false;
	form24.required = false;
	form21.disable = true;
	form22.disable = true;
	form23.disable = true;
	form24.disable = true;
	$(".card").addClass("current");
	$(".paypal").removeClass("current");
	$(".alipay").removeClass("current");
}

/*
 * When user clicks paypal payment
 * method, the paypal box is displayed and set as current, the
 * credit card form and alipay info box are hidden
 * and the form is no longer required
 */
function paypal() {
	a1.style.display = 'none';
	a2.style.display = 'none';
	a3.style.display = 'none';
	a4.style.display = 'none';
	a5.style.display = 'none';
	b.style.display = 'block';
	c.style.display = 'none';
	d.style.display = 'none';
	x1.style.display = 'none';
	x2.style.display = 'none';
	x3.style.display = 'none';
	form11.required = false;
	form12.required = false;
	form13.required = false;
	form14.required = false;
	form15.required = false;
	form21.required = false;
	form22.required = false;
	form23.required = false;
	form24.required = false;

	// clear form
	form11.value = "";
	form12.value = "";
	form13.value = "";
	form14.value = "";
	form15.value = "";
	form21.value = "";
	form22.value = "";
	form23.value = "";
	form24.value = "";
	form25.value = "";
	form26.value = "";
	form27.value = "";

	$(".card").removeClass("current");
	$(".paypal").addClass("current");
	$(".alipay").removeClass("current");
}

/*
 * When user clicks paypal payment
 * method, the alipay box is displayed set as current, the
 * credit card form and paypal info box are hidden
 * and the form is no longer required
 */
function alipay() {
	a1.style.display = 'none';
	a2.style.display = 'none';
	a3.style.display = 'none';
	a4.style.display = 'none';
	a5.style.display = 'none';
	b.style.display = 'none';
	c.style.display = 'block';
	d.style.display = 'none';
	x1.style.display = 'none';
	x2.style.display = 'none';
	x3.style.display = 'none';
	form11.required = false;
	form12.required = false;
	form13.required = false;
	form14.required = false;
	form15.required = false;
	form21.required = false;
	form22.required = false;
	form23.required = false;
	form24.required = false;
	// clear form
	form11.value = "";
	form12.value = "";
	form13.value = "";
	form14.value = "";
	form15.value = "";
	form21.value = "";
	form22.value = "";
	form23.value = "";
	form24.value = "";
	form25.value = "";
	form26.value = "";
	form27.value = "";

	$(".card").removeClass("current");
	$(".paypal").removeClass("current");
	$(".alipay").addClass("current");
}


/*
 * When user clicks to use the same billing
 * address, the secondary form is hidden and
 * no longer required
 */
function same() {
	x3.style.display = 'none';
	d.style.display = 'block';
	e.style.display = 'none';
	f.style.display = 'block';
	form11.required = true;
	form12.required = true;
	form13.required = true;
	form14.required = true;
	form15.required = true;
	form21.required = false;
	form22.required = false;
	form23.required = false;
	form24.required = false;
	// clear form
	form21.value = "";
	form22.value = "";
	form23.value = "";
	form24.value = "";
	form25.value = "";
	form26.value = "";
	form27.value = "";
}

/*
 * When user clicks to use a different billing
 * address, the secondary form is displated and
 * required fileds are set
 */
function different() {
	x3.style.display = 'block';
	d.style.display = 'none';
	e.style.display = 'block';
	f.style.display = 'none';
	form11.required = true;
	form12.required = true;
	form13.required = true;
	form14.required = true;
	form15.required = true;
	form21.required = true;
	form22.required = true;
	form23.required = true;
	form24.required = true;
}
