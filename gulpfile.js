var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    connect = require('gulp-connect');
    htmlmin = require('gulp-html-minifier');



gulp.task('html', function() {
	return gulp.src('index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(htmlmin({removeComments: true}))
    .pipe(gulp.dest('html'))
	.pipe(connect.reload());
});

gulp.task('sass', function() {
    return gulp.src('styles/sass/main.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('styles/css'))
		.pipe(connect.reload());
});

gulp.task('js', function() {
    return gulp.src('javascript/dev/javascript.js')
    	.pipe(uglify())
        .pipe(gulp.dest('javascript/prod'))
		.pipe(connect.reload());
});

gulp.task('form', function() {
    gulp.src('form.php')
	.pipe(connect.reload());
});

gulp.task('watch', function() {
	gulp.watch('*.html', ['html']);
	gulp.watch('styles/sass/*.scss', ['sass']);
	gulp.watch('javascript/dev/*.js', ['js']);
	gulp.watch('form.php', ['form']);
});

gulp.task('connect', function() {
	connect.server({
		root: '',
		livereload: true
	});
});


gulp.task('default', ['sass', 'js', 'html', 'watch', 'connect']);

